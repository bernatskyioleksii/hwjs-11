
const eyes = document.querySelectorAll('.icon-password');

eyes.forEach(eye => {
    eye.addEventListener('click', () => {
        eye.classList.toggle('fa-eye-slash')
        const input = eye.previousElementSibling
        if (input.type === 'password') {
            input.type = 'text'
        } else {
            input.type = 'password'
        }
    })
});

const form = document.querySelector('.password-form');
const error = form.querySelector('.error')
form.addEventListener('submit', (event) => {
    event.preventDefault()
    if (form.password.value === form.confirmPassword.value) {
        error.textContent = '';
        alert('You are welcome')
        form.reset()
        form.password.focus()
    } else {
        error.textContent = 'Потрібно ввести однакові значення';
    }
})
// =====================================================================


